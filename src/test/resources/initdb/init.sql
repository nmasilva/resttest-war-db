CREATE DATABASE sampledb
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
CONNECTION LIMIT = -1;
\connect sampledb

CREATE TABLE greeting (
    id            BIGINT       PRIMARY KEY,
    subject       CHARACTER VARYING(255)         
);

INSERT INTO greeting (id, subject) VALUES(1, 'World');