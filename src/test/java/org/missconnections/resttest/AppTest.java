package org.missconnections.resttest;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
  @Test(expected=IllegalArgumentException.class)
  public void mainAppIllegalArgumentException() {
    App.main(null);
  }
}
