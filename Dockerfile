FROM tomcat:latest

RUN ["rm", "-fr", "/usr/local/tomcat/webapps/ROOT"]
COPY ./target/hello.war /usr/local/tomcat/webapps/ROOT.war

CMD ["catalina.sh", "run"]