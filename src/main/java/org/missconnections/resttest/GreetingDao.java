package org.missconnections.resttest;

import org.springframework.data.repository.CrudRepository;

interface GreetingDao extends CrudRepository<Greeting, Long> {
}