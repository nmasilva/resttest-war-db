package org.missconnections.resttest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

public class GreetingServiceTest {
  @Test
  public void getSubjectSuccess() {
    Greeting greeting = createGreetingService().getGreeting().get();
    
    assertEquals(1L, greeting.getId());
    assertEquals("World", greeting.getSubject());
  }
  
  @Test
  public void setSubjectSuccess() {
    GreetingService greetingService = createGreetingService();
    greetingService.updateGreeting("Nuno");
    
    Greeting greeting = greetingService.getGreeting().get();
    assertEquals(1L, greeting.getId());
    assertEquals("Nuno", greeting.getSubject());
  }
  
  private GreetingService createGreetingService() {
    GreetingService greetingService = new GreetingService();
    ReflectionTestUtils.setField(greetingService, "greetingDao", new TestableGreetingDao());
    
    return greetingService;
  }
  
  private class TestableGreetingDao implements GreetingDao {
    String storedValue = "World";
    
    @Override
    public <S extends Greeting> S save(S entity) {
      storedValue = entity.getSubject();
      
      return entity;
    }

    @Override
    public <S extends Greeting> Iterable<S> saveAll(Iterable<S> entities) {
      return null;
    }

    @Override
    public Optional<Greeting> findById(Long id) {
      return Optional.of(new Greeting(id, storedValue));
    }

    @Override
    public boolean existsById(Long id) {
      return false;
    }

    @Override
    public Iterable<Greeting> findAll() {
      return null;
    }

    @Override
    public Iterable<Greeting> findAllById(Iterable<Long> ids) {
      return null;
    }

    @Override
    public long count() {
      return 0;
    }

    @Override
    public void deleteById(Long id) {
    }

    @Override
    public void delete(Greeting entity) {
    }

    @Override
    public void deleteAll(Iterable<? extends Greeting> entities) {
    }

    @Override
    public void deleteAll() {
    }

	@Override
	public void deleteAllById(Iterable<? extends Long> ids) {
	}
  }
}