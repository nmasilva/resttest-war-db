package org.missconnections.resttest;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Greeting {
	@Id
	private long id;
	private String subject;

	public Greeting() {
	}

	public Greeting(String subject) {
		this.subject = subject;
	}

	public Greeting(long id, String subject) {
		this.id = id;
		this.subject = subject;
	}

	public long getId() {
		return id;
	}

	public String getSubject() {
		return subject;
	}
}