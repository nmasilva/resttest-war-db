package org.missconnections.resttest;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class GreetingService {
	@Autowired
	private GreetingDao greetingDao;

	public Optional<Greeting> getGreeting() {
		return greetingDao.findById((long) 1);
	}

	@Transactional
	public void updateGreeting(String subject) {
		greetingDao.save(new Greeting(1, subject));
	}
}