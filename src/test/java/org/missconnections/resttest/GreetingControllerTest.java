package org.missconnections.resttest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Optional;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

public class GreetingControllerTest {

  @Test
  public void getgreetingControllerSuccess() {
    ResponseEntity<GreetingDto> greeting = createGreetingController(createGreetingService()).greeting();
    
    assertNotNull(greeting);
    assertEquals(HttpStatus.OK, greeting.getStatusCode()); 
    assertEquals(1L, greeting.getBody().getId());
    assertEquals("Hello, World!", greeting.getBody().getMessage());
  }
  
  @Test
  public void getgreetingControllerInternalServerError() {
    ResponseEntity<GreetingDto> greeting = createGreetingController(createGreetingService500()).greeting();
    
    assertNotNull(greeting);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, greeting.getStatusCode()); 
  }
  
  @Test
  public void setgreetingControllerSuccess() {
    GreetingController greetingController = createGreetingController(createGreetingService());
    greetingController.setGreetingSubject("Nuno");
    
    ResponseEntity<GreetingDto> greeting = greetingController.greeting();
    
    assertNotNull(greeting);
    assertEquals(HttpStatus.OK, greeting.getStatusCode());
    assertEquals(1L, greeting.getBody().getId());
    assertEquals("Hello, Nuno!", greeting.getBody().getMessage());
  }
  
  private GreetingController createGreetingController(GreetingService greetingService) {
    GreetingController greetingController = new GreetingController();
    ReflectionTestUtils.setField(greetingController, "greetingService", greetingService);
    
    return greetingController;
  }
  
  private GreetingService createGreetingService500() {
    GreetingService greetingService = new GreetingService() {
      @Override
      public Optional<Greeting> getGreeting() {
        return Optional.empty();
      }

      @Override
      public void updateGreeting(String subject) {
      }
    };
    
    return greetingService;
  }
  
  private GreetingService createGreetingService() {
    GreetingService greetingService = new GreetingService() {
      String storedValue = "World";
      
      @Override
      public Optional<Greeting> getGreeting() {
        return Optional.of(new Greeting(storedValue));
      }

      @Override
      public void updateGreeting(String subject) {
        storedValue = subject;
      }
    };
    
    return greetingService;
  }
}