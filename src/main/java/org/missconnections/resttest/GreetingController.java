package org.missconnections.resttest;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {
	private static final String TEMPLATE = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@Autowired
	private GreetingService greetingService;

	@GetMapping("/greeting")
	public ResponseEntity<GreetingDto> greeting() {
		ResponseEntity<GreetingDto> responseEntity;

		Optional<Greeting> greeting = greetingService.getGreeting();
		if (greeting.isPresent()) {
			responseEntity = ResponseEntity.ok(new GreetingDto(counter.incrementAndGet(), String.format(TEMPLATE, greeting.get().getSubject())));
		} else {
			responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		return responseEntity;
	}

	@GetMapping("/greeting/{subject}")
	public void setGreetingSubject(@PathVariable String subject) {
		greetingService.updateGreeting(subject);
	}
}